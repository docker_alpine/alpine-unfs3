# alpine-unfs3
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-unfs3)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-unfs3)

### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-unfs3/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-unfs3/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [UNFS3](http://unfs3.sourceforge.net/)
    - UNFS3 is a user-space implementation of the NFSv3 server specification.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 111:111/tcp \
           -p 111:111/ucp \
           -p 2049:2049/tcp \
           -p 2049:2049/ucp \
           -v /data:/data \
           -e NFS_SHARE="/data,..." \
           forumi0721/alpine-unfs3:[ARCH_TAG]
```



----------------------------------------
#### Usage

* mount share directory
    - Linux : `mount -t nfs localhost:/data /mnt/data` 
    - Darwin : `mount -t nfs -o resvport,rw localhost:/data /Volumes/data`



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 111/tcp            | Portmap port                                     |
| 111/udp            | Portmap port                                     |
| 2049/tcp           | nfs port                                         |
| 2049/udp           | nfs port                                         |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /data              | nfs share directory                              |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| NFS_SHARE          | nfs share directory (default : /data)            |
| NFS_OPTION         | nfs share option (default : rw,no_root_squash)   |

